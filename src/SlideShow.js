import React, { Component } from 'react';

import Slide from './Slide';
import './slideShow.css';

class SlideShow extends Component {
  constructor(props) {
    super(props)

    this.state = {
      images: [
        "/images/01.jpg",
        "/images/02.jpg",
        "/images/03.jpg",
        "/images/04.jpg",
        "/images/05.jpg",
        "/images/06.jpg",
        "/images/07.jpg",
      ],
      currentIndex: 0,
      slideDir: "to-right"
    }
  }

  goToPrevSlide = () => {
    
      if(this.state.currentIndex === 0) {
        return this.setState({
          currentIndex: this.state.images.length - 1,
        })
      }

    this.setState(prevState => ({
      currentIndex: prevState.currentIndex - 1,
      slideDir: "to-left",
    }))
  }

  goToNextSlide = () => {
    if(this.state.currentIndex === this.state.images.length - 1) {
      return this.setState({
        currentIndex: 0,
      })
    }
    
    this.setState(prevState => ({
      currentIndex: prevState.currentIndex + 1,
      slideDir: "to-right",
    }))
  }

  slideWidth = () => {
     return document.querySelector('.slide').clientWidth
  }

  render() {
    return (
      <div className="slide-show">

        <div className="slide-show-container">
            {
              this.state.images.map((image, i) => {
                const show = (this.state.currentIndex === i) ? 1: 0;
                return <Slide key={i} image={image}  show={show} slideDir={this.state.slideDir}></Slide>
            })
            }
        </div>

        <LeftArrow
         goToPrevSlide={this.goToPrevSlide}
        />

        <RightArrow
         goToNextSlide={this.goToNextSlide}
        />
      </div>
    );
  }
}

const LeftArrow = (props) => {
  return (
    <div className="backArrow arrow" onClick={props.goToPrevSlide}>
      <i className="fa fa-arrow-left fa-2x" aria-hidden="true"></i>
    </div>
  );
}


const RightArrow = (props) => {
  return (
    <div className="nextArrow arrow" onClick={props.goToNextSlide}>
      <i className="fa fa-arrow-right fa-2x" aria-hidden="true"></i>
    </div>
  );
}

export default SlideShow;